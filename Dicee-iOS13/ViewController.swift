//  ViewController.swift
//  Dicee-iOS13
//
//  Created by Angela Yu on 11/06/2019.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var DiceImageViewone: UIImageView!
    @IBOutlet weak var DiceImageView2: UIImageView!
    
    var leftDiceNumber = 1
    var RightDiceNumber = 5
    
    //    in order to change a name of a variable press ctrl+rightClick go to the refactor and rename.
//    agar hum aisay nhi kareingy to aur just samny sy change krleng to har jaga sy variable change nhi hoga aur code nhi chaley ga aur error dey deyga.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        //    agar me apnay is first dice ko six wala dice show karana chaho to
//             who.what = value
//kis cheez me changing = konsi property me changing=aurkiya set krna hai
//                WHO   .WHAT  = VALUE
//        DiceImageViewone.image = UIImage(named: "DiceSix")
//        DiceImageViewone.alpha = 0.5
//        now changing for image2
//        DiceImageView2.image = UIImage(named: "DiceTwo")
    
    
    }
    @IBAction func RollButtonPressed(_ sender: UIButton) {
/*        print("Roll button has pressed")
        DiceImageViewone.image = UIImage(named: "DiceFour")
        DiceImageView2.image = UIImage(named: "DiceFour")
 */
//        let DiceImages = [UIImage(named:"DiceOne"),UIImage(named:"DiceTwo"),UIImage(named:"DiceThree"),UIImage(named:"DiceFour"),UIImage(named:"DiceFive"),UIImage(named:"DiceSix")]

        let DiceImagesName = ["DiceOne","DiceTwo", "DiceThree","DiceFour", "DiceFive","DiceSix"]
        
        DiceImageViewone.image = UIImage(named: DiceImagesName.randomElement() ?? "")
        
        DiceImageView2.image = UIImage(named: DiceImagesName.randomElement() ?? "")
        
    }

   /*@IBAction func ResetButtonPressed(_ sender: UIButton) {
        DispatchQueue.main.async { [weak self] in
            self?.DiceImageViewone.image = UIImage(named: "DiceOne")
            self?.DiceImageView2.image = UIImage(named: "DiceOne")
        }
    }*/
}
